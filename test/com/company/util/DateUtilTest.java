package com.company.util;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Classe de testeig unitari de la classe <code>DateUtil</code>
 *
 * Cada cas de prova definit al pla de prova dels métodes definits a <code>DateUtil</code> està implementat com un
 * métode en aquesta classe.
 *
 * Per exemple, el cas de prova número 1 del métode <code>nomDelMes</code> de <code>DateUtil</code> està implementat
 * al métode <code>obteNomDelMes1</code> d'aquesta classe, el segon cas de prova al métode <code>obteNomDelMes2</code>
 * i així succesivament.
 *
 * El mateix s'ha fet amb els casos de prova del pla de proves del métode <code>obteHoraDelJapo</code> de
 * <code>DateUtil</code>
 *
 * @see DateUtil
 * @see DateUtil#obteNomDelMes(int)
 * @see DateUtil#obteHoraDelJapo(int, int) 
 */
public class DateUtilTest {

    @Test(expected = IllegalArgumentException.class)
    public void obteNomDelMes1() {
        DateUtil dateUtil = new DateUtil();
        dateUtil.obteNomDelMes(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void obteNomDelMes2() {
        DateUtil dateUtil = new DateUtil();
        dateUtil.obteNomDelMes(-10);
    }

    @Test()
    public void obteNomDelMes3() {
        DateUtil dateUtil = new DateUtil();
        Assert.assertEquals("Gener", dateUtil.obteNomDelMes(1));
    }

    @Test()
    public void obteNomDelMes4() {
        DateUtil dateUtil = new DateUtil();
        Assert.assertEquals("Desembre", dateUtil.obteNomDelMes(12));
    }

    @Test(expected = IllegalArgumentException.class)
    public void obteNomDelMes5() {
        DateUtil dateUtil = new DateUtil();
        Assert.assertEquals("Desembre", dateUtil.obteNomDelMes(13));
    }

    @Test(expected = IllegalArgumentException.class)
    public void obteNomDelMes6() {
        DateUtil dateUtil = new DateUtil();
        Assert.assertEquals("Desembre", dateUtil.obteNomDelMes(100));
    }

    @Test
    public void obteHoraDelJapo1() {
        DateUtil dateUtil = new DateUtil();
        Assert.assertEquals("07:50", dateUtil.obteHoraDelJapo(23,50));
    }

    @Test(expected = IllegalArgumentException.class)
    public void obteHoraDelJapo2() {
        DateUtil dateUtil = new DateUtil();
        dateUtil.obteHoraDelJapo(-10, 50);
    }

    @Test(expected = IllegalArgumentException.class)
    public void obteHoraDelJapo3() {
        DateUtil dateUtil = new DateUtil();
        dateUtil.obteHoraDelJapo(100, 50);
    }

    @Test(expected = IllegalArgumentException.class)
    public void obteHoraDelJapo4() {
        DateUtil dateUtil = new DateUtil();
        dateUtil.obteHoraDelJapo(1, -10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void obteHoraDelJapo5() {
        DateUtil dateUtil = new DateUtil();
        dateUtil.obteHoraDelJapo(1, 100);
    }
}