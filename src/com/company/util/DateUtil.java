package com.company.util;

/**
 *
 * Classe que implementa les següents utilitats relacionades amb les dates. 
 *  - donat un número de mes, retorna el seu nom en català
 *  - donada una hora a Espanya retorna l'hora oficial al Japó
 *
 * @author José Luis Álvarez Casas
 * @version v1.0 (18/03/2021)
 * @see #obteNomDelMes(int) 
 * @see #obteHoraDelJapo(int, int)
 */
public class DateUtil {

    /**
     *  Donat un número de mes el métode retorna el nom en català del mes corresponent.
     *
     *  Si el número de mes proporcionat és menor que 1 i més gran que 12, el métode retorna una <code>IllegalArgumentException</code>.
     *
     * @param numDelMes número enter entre 1 i 12
     * @return <code>String</code> amb el nom del mes corresponent al paràmetre proporcionat.
     */
    public String obteNomDelMes(int numDelMes) {
        if (numDelMes <= 0 || numDelMes >= 13) {
            throw new IllegalArgumentException("El número del mes ha d'estar a l'interval [1,12] !");
        }
        String[] nomMesos= {"Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"};
        return nomMesos[numDelMes -1];
    }

    /**
     * Retorna el format de String l'hora i minuts al Japó a partir dels paràmetres proporcionats que respresenten
     * l'hora a Espanya.
     *
     * El métode retorna una <code>IllegalArgumentException</code> si el paràmetre <code>hora</code> és
     * menor que 0 o més gran que 23 o si el paràmetre <code>minuts</code> és menor que 0 o més gran que 59.
     *
     * @param hora enter entre 0 i 23 corresponent a l'hora
     * @param minuts enter entre 0 i 59 corresponent als minuts
     * @return <code>String</code> en format <code>hh:mm</code>, essent <code>hh</code> l'hora al Japó calculada a partir de l'indicada pel
     * paràmetre <code>hora</code> i essent <code>mm</code> els minuts corresponents.
     */
    public String obteHoraDelJapo(int hora, int minuts) {
        if (hora < 0 || hora > 23) {
            throw new IllegalArgumentException("L'hora ha d'estar a l'interval [0,23] !");
        }

        if (minuts < 0 || minuts > 59) {
            throw new IllegalArgumentException("Els minuts han d'estar a l'interval [0,59] !");
        }

        int horaJaponesa = (hora + 8) % 24;
        String strHoraAJapo = horaJaponesa + ":" + minuts;
        if (horaJaponesa < 10) {
            strHoraAJapo = "0" + horaJaponesa + ":" + minuts;
        }
        return strHoraAJapo;
    }
}
